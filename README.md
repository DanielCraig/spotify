Spotify project written in React and Node.JS for the server.

Use `cd FE/Spotify` and `npm start` to start the React Front-End

For the server `cd authorization_code` and `nodemon app.js` 

The App currently fetches user info and user playlists onComponentDidMount.
Authorization is done via Spotify API by using Bearer Token. 
Once received, the token is set in the cookies with a 1-hour expiry. 

//TODO
* [ ] Handle token refresh on expiry (Node.JS)
* [ ] Add a sidebar which will contain all playlists of current user
* [ ] When a playlist is clicked on the sidebar, display all tracks of that playlist in the main page.
* [ ] All pages must be dark
* [ ] Track search
* [ ] Finish styling the Profile Page
* [ ] Handle Logout (clear cookies)
* [ ] Move playlist fetch to main component


Thank you for coming here and taking a look at my project.
This is just for Experimenting and Study purposes. 

If you want, show me some love and add a Star :) 

Feel free to contact me on LinkedIn for any question. 
